use std::fs;
use std::io::{self, BufRead};
use std::path::Path;

const INDENT: usize = 4;

fn usage() {
    eprintln!("concatsrc.exe -src <srcfile> -dst <dstfile>");
}

fn get_files() -> Result<(String, String), String> {
    let args = std::env::args();
    let mut args = args.skip(1);

    let mut src_file = None;
    let mut dst_file = None;

    while let Some(arg) = args.next() {
        match arg.as_str() {
            "-src" => src_file = args.next(),
            "-dst" => dst_file = args.next(),
            s => return Err(format!("invalid arg: {}", s)),
        }
    }

    let src = src_file.take().ok_or_else(|| "no src".to_string())?;
    let dst = dst_file.take().ok_or_else(|| "no dst".to_string())?;

    Ok((src, dst))
}

fn main() -> io::Result<()> {
    let (src, dst) = match get_files() {
        Ok(res) => res,
        Err(err) => {
            eprintln!("ERROR: {}", err);
            usage();
            return Ok(());
        }
    };

    let mut dst = fs::File::create(dst)?;

    parse(&src, 0, &mut dst)
}

fn parse<W: io::Write>(src: &str, nest: usize, dst: &mut W) -> io::Result<()> {
    let path = Path::new(src);
    let src = fs::File::open(src)?;
    let mut reader = io::BufReader::new(src);

    let mut line = String::new();
    loop {
        line.clear();
        if reader.read_line(&mut line)? == 0 {
            break;
        }

        {
            let trimed_line = line.trim();
            if trimed_line.starts_with("mod ") {
                if let Some(pos) = trimed_line.find(';') {
                    let mod_name = trimed_line[4..pos].to_owned();
                    if nest > 0 {
                        write!(dst, "{:width$}", " ", width = nest * INDENT)?;
                    }
                    writeln!(dst, "mod {} {{", mod_name)?;
                    if nest == 0 {
                        let mut mod_file = path.with_file_name(&mod_name);
                        mod_file.set_extension("rs");
                        if let Some(mod_file) = mod_file.to_str() {
                            parse(mod_file, nest + 1, dst)?;
                        }
                    } else {
                        let mut mod_file = path.with_extension("");
                        mod_file.push(&mod_name);
                        mod_file.set_extension("rs");
                        if let Some(mod_file) = mod_file.to_str() {
                            parse(mod_file, nest + 1, dst)?;
                        }
                    }
                    if nest > 0 {
                        write!(dst, "{:width$}", " ", width = nest * INDENT)?;
                    }
                    if trimed_line.len() > pos + 1 {
                        let len = trimed_line.len();
                        let rest = trimed_line[pos + 1..len].to_owned();
                        writeln!(dst, "}} {}", rest)?;
                    } else {
                        writeln!(dst, "}}")?;
                    }
                    continue;
                }
            } else if trimed_line.starts_with("pub") {
                if let Some((pos0, _)) = trimed_line.match_indices(" mod ").next() {
                    if let Some(pos1) = trimed_line.find(';') {
                        let vis = trimed_line[0..pos0].to_owned();
                        let mod_name = trimed_line[pos0 + 5..pos1].to_owned();
                        if nest > 0 {
                            write!(dst, "{:width$}", " ", width = nest * INDENT)?;
                        }
                        writeln!(dst, "{} mod {} {{", vis, mod_name)?;
                        if nest == 0 {
                            let mut mod_file = path.with_file_name(&mod_name);
                            mod_file.set_extension("rs");
                            if let Some(mod_file) = mod_file.to_str() {
                                parse(mod_file, nest + 1, dst)?;
                            }
                        } else {
                            let mut mod_file = path.with_extension("");
                            mod_file.push(&mod_name);
                            mod_file.set_extension("rs");
                            if let Some(mod_file) = mod_file.to_str() {
                                parse(mod_file, nest + 1, dst)?;
                            }
                        }
                        if nest > 0 {
                            write!(dst, "{:width$}", " ", width = nest * INDENT)?;
                        }
                        if trimed_line.len() > pos1 + 1 {
                            let len = trimed_line.len();
                            let rest = trimed_line[pos1 + 1..len].to_owned();
                            writeln!(dst, "}} {}", rest)?;
                        } else {
                            writeln!(dst, "}}")?;
                        }
                        continue;
                    }
                }
            }
        }

        if nest > 0 {
            write!(dst, "{:width$}", " ", width = nest * INDENT)?;
        }
        writeln!(dst, "{}", line.trim_end())?;
    }

    Ok(())
}
