concatsrc
=========

Rustでmodでファイル分割したやつを１つのファイルにまとめる（雑）  
用途は…  




コマンドラインから
```bash
concatsrc.exe -src project/src/main.rs -dst temp/project.rs
```
とかして実行する
この例の場合、 main.rs から関連モジュールまとめて project.rs に書き出す  
フラグの -src と -dst は指定必須  





```rust
mod foo;
```
のようなシンプルな記述でのモジュール指定が１つあるだけの行を見つけたら


```rust
mod foo {
   ...
}
```
foo.rsの中身を括弧内にぶちまける








    
以下のはいずれも対応してない




例えば mod.rs という名前を使ってモジュールのファイルを示すやつ
(以前のRustのバージョンのモジュールの書き方？)



例えばモジュール名がrawで指定されてる
```rust
mod r#foo;
```



例えば1行に複数個ある
```rust
mod foo; mod bar; mod baz;
```



例えばモジュール名が改行されてる
```rust
mod
foo;
```



例えばコメントが割り込んでる
```rust
/* comment */ mod foo;
```



例えばモジュール名とファイル名が異なる名前になってる
```rust
#[path = "bar.rs"]
mod foo;
```


思いつけたものだけを列挙したが、他にも無理なパターンありそう


